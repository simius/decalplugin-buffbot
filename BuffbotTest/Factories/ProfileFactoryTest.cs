﻿using Buffbot.Contracts;
using Buffbot.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BuffbotTest.Factories
{
  [TestClass]
  public class ProfileFactoryTest
  {
    private string validData = "[{\"name\": \"test profile 1\",\"synonymes\": [ \"tp1\", \"test1\", \"one\" ],\"families\": [ \"SpellFamily1\", \"SpellFamily2\" ]},{\"name\": \"test profile 2\",\"synonymes\": [ \"tp2\", \"test2\", \"two\" ],\"families\": [ \"SpellFamily2\", \"SpellFamily3\" ]}]";
    private string invalidData = "[{\"name\": \"test profile 1\",\"synonymes\": [ \"tp1\", \"test1\", \"one\" ],\"families\": [ \"SpellFamily1\", \"SpellFamily2\" ]},{\"name\": \"test profile 2\",\"synonymes\": [ \"tp2\", \"test2\", \"two\", \"tp1\" ],\"families\": [ \"SpellFamily2\", \"SpellFamily3\" ]}]";

    [TestMethod, ExpectedException(typeof(System.IO.InvalidDataException))]
    public void TestInitFail()
    {
      ProfileFactory.InitFactory(this.invalidData, true);
    }

    [TestMethod]
    public void TestInitSuccess()
    {
      ProfileFactory.InitFactory(this.validData, true);
    }

    [TestMethod]
    public void TestGetByName()
    {
      ProfileFactory.InitFactory(this.validData, true);

      IProfile p1 = ProfileFactory.GetByName("test profile 1");
      IProfile p2 = ProfileFactory.GetByName("test profile 2");

      Assert.IsNotNull(p1);
      Assert.IsNotNull(p2);

      Assert.AreEqual(3, p1.Synonymes.Count);
      Assert.AreEqual(3, p2.Synonymes.Count);
    }

    [TestMethod]
    public void TestGetBySynonyme()
    {
      ProfileFactory.InitFactory(this.validData, true);

      IProfile p1 = ProfileFactory.GetByName("tp1");
      IProfile p1X2 = ProfileFactory.GetByName("one");

      Assert.AreEqual(p1.Name, p1X2.Name);

      IProfile p2 = ProfileFactory.GetByName("test2");
      Assert.AreNotEqual(p1.Name, p2.Name);
    }
  }
}
