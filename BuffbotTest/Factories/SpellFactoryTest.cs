﻿using System.Collections.Generic;
using System.Linq;
using Buffbot.Contracts;
using Buffbot.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BuffbotTest.Factories
{
  [TestClass]
  public class SpellFactoryTest
  {
    private static string testData = "{\"TestFamily\": [{\"level\": 1,\"name\": \"Test name I\",\"id\": 52},{\"level\": 2,\"name\": \"Test name II\",\"id\":  57}],\"TestFamily 2\": [{\"level\": 1,\"name\": \"Test2 name I\",\"id\": 93},{\"level\": 2,\"name\": \"Test2 name II\",\"id\":  125}]}";

    [TestMethod]
    public void TestGetSpellByName()
    {
      SpellFactory.Init(testData, true);

      Assert.AreEqual(52, SpellFactory.GetSpell("Test name I").Id);
      Assert.AreEqual(2, SpellFactory.GetSpell("Test name II").Level);
    }

    [TestMethod]
    public void TestGetSpellById()
    {
      SpellFactory.Init(testData, true);

      Assert.AreEqual("Test name I", SpellFactory.GetSpell(52).Name);
      Assert.AreEqual(2, SpellFactory.GetSpell(125).Level);
    }

    [TestMethod]
    public void TestGetSpellsByFamily()
    {
      SpellFactory.Init(testData, true);

      IList<ISpell> spells = SpellFactory.GetSpellsByFamily("TestFamily 2");

      Assert.AreEqual(2, spells.Count);
      Assert.IsNotNull(spells.FirstOrDefault(s => s.Name == "Test2 name I"));
      Assert.IsNotNull(spells.FirstOrDefault(s => s.Name == "Test2 name II"));
      Assert.IsNull(spells.FirstOrDefault(s => s.Name == "NoneExistant"));
    }

  }
}
