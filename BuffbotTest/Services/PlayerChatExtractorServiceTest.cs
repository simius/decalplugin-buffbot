﻿using Buffbot.Chat;
using Buffbot.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BuffbotTest.Services
{
  [TestClass]
  public class PlayerChatExtractorServiceTest
  {

    private readonly string invalid = "<Tell:IIDString::>Test message<\\Tell>";
    private readonly string valid = "<Tell:IIDString:12345:TestName>Test message<\\Tell>";

    [TestMethod]
    public void TestExtractPlayerChatFail()
    {
      Assert.IsNull(PlayerChatExtractorService.GetFromMessage(this.invalid));
    }

    [TestMethod]
    public void TestExtractPlayerChatSuccess()
    {
      PlayerChat chat = PlayerChatExtractorService.GetFromMessage(this.valid);

      Assert.IsNotNull(chat);
      Assert.AreEqual("Test message", chat.Message);
      Assert.AreEqual("TestName", chat.Name);
      Assert.AreEqual(12345, chat.Guid);
    }
  }
}
