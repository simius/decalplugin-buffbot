﻿using Buffbot.Bot;
using Buffbot.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BuffbotTest.Bot
{
  [TestClass]
  public class SpellTest
  {

    [TestMethod]
    public void TestName()
    {
      ISpell s = new Spell(1, "test", 1);
      Assert.AreEqual("test", s.Name);
    }

    [TestMethod]
    public void TestId()
    {
      ISpell s = new Spell(5, "test", 1);
      Assert.AreEqual(5, s.Id);
    }

  }
}
