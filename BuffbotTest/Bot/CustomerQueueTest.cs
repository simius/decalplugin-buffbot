﻿using System;
using Buffbot.Bot;
using Buffbot.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BuffbotTest.Bot
{
  [TestClass]
  public class CustomerQueueTest
  {

    #region Customer.

    internal class Customer : ICustomer
    {
      public int Guid { get ; set; }

      public int Count { get; set; }

      public IProfile GetNext()
      {
        throw new NotImplementedException();
      }

      public int AddProfile(IProfile profile)
      {
        throw new NotImplementedException();
      }

      public bool RemoveProfile(IProfile profile)
      {
        throw new NotImplementedException();
      }

      public IProfile[] ClearQueue()
      {
        throw new NotImplementedException();
      }
    }

    #endregion
    
    [TestMethod]
    public void TestAdd()
    {
      CustomerQueue q = new CustomerQueue();
      Assert.AreEqual(1, q.AddCustomer(new Customer()));
      ICustomer c = new Customer() {Guid = 1};
      Assert.AreEqual(2, q.AddCustomer(c));
      Assert.AreEqual(-1, q.AddCustomer(c));
    }

    [TestMethod]
    public void TestRemoveWithoutEntry()
    {
      CustomerQueue q = new CustomerQueue();
      Assert.IsFalse(q.RemoveCustomer(new Customer()));
    }

    [TestMethod]
    public void TestRemoveWithEntry()
    {
      CustomerQueue q = new CustomerQueue();
      ICustomer c = new Customer();
      q.AddCustomer(c);
      Assert.IsTrue(q.RemoveCustomer(c));
    }

    [TestMethod]
    public void TestGetNext()
    {
      CustomerQueue q = new CustomerQueue();
      
      // Add a couple of entries.
      q.AddCustomer(new Customer() {Guid = 1});
      q.AddCustomer(new Customer() {Guid = 2});

      Assert.AreEqual(1, q.GetNext().Guid);
      Assert.AreEqual(2, q.GetNext().Guid);
      Assert.IsNull(q.GetNext());
    }

    [TestMethod]
    public void TestCount()
    {
      CustomerQueue q = new CustomerQueue();
      Assert.AreEqual(0, q.Count);
      q.AddCustomer(new Customer());
      Assert.AreEqual(1, q.Count);
      ICustomer c = new Customer();
      q.AddCustomer(c);
      Assert.AreEqual(2, q.Count);
      q.RemoveCustomer(c);
      Assert.AreEqual(1, q.Count);
    }

    [TestMethod]
    public void TestClearQueue()
    {
      CustomerQueue q = new CustomerQueue();
      ICustomer c = new Customer() {Guid = 5};
      q.AddCustomer(c);
      Assert.AreEqual(1, q.Count);
      Assert.AreEqual(c, q.ClearQueue()[0]);
      Assert.AreEqual(0, q.Count);
    }

  }
}
