﻿using System;
using System.Collections.Generic;
using Buffbot.Bot;
using Buffbot.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BuffbotTest.Bot
{
  [TestClass]
  public class ProfileTest
  {
    #region Spell

    public class Spell : ISpell
    {
      public int Id
      {
        get { throw new NotImplementedException(); }
      }

      public string Name
      {
        get { throw new NotImplementedException(); }
      }

      public short Level
      {
        get { throw new NotImplementedException(); }
      }
    }

    #endregion

    [TestMethod]
    public void TestName()
    {
      IProfile p = new Profile("TestProfile", new List<string>());
      Assert.AreEqual("TestProfile", p.Name);
    }

    [TestMethod]
    public void TestNameSynonymes()
    {
      IProfile p = new Profile("TestProfile", new List<string>(), new string[] { "test", "test2" });
      Assert.IsTrue(p.Synonymes.Contains("test"));
      Assert.IsTrue(p.Synonymes.Contains("test2"));
    }

    [TestMethod]
    public void TestCount()
    {
      IProfile p = new Profile("TestProfile", new List<string>());
      p.AddSpell(new Spell());
      Assert.AreEqual(1, p.Count);
      p.AddSpell(new Spell());
      Assert.AreEqual(2, p.Count);
    }

    [TestMethod]
    public void TestGetNext()
    {
      IProfile p = new Profile("TestProfile", new List<string>());
      
      ISpell s1 = new Spell();
      ISpell s2 = new Spell();
      
      p.AddSpell(s1);
      p.AddSpell(s2);

      Assert.AreEqual(2, p.Count);
      Assert.AreSame(s1, p.GetNext());
      Assert.AreEqual(1, p.Count);
      Assert.AreSame(s2, p.GetNext());
    }

    [TestMethod]
    public void TestAddSpells()
    {
      IProfile p = new Profile("test", new List<string>());
      Assert.AreEqual(0, p.Count);

      IList<ISpell> spells = new List<ISpell>();

      spells.Add(new Spell());
      spells.Add(new Spell());

      p.AddSpells(spells);

      Assert.AreEqual(2, p.Count);

      IList<ISpell> spells2 = new List<ISpell>();

      spells2.Add(new Spell());
      spells2.Add(new Spell());
      spells2.Add(spells[0]);

      // The last one should be equal to one of the spells already in the profile, so it should not be added.
      p.AddSpells(spells2);

      Assert.AreEqual(4, p.Count);
    }

    [TestMethod]
    public void TestAddSpell()
    {
      IProfile p = new Profile("test", new List<string>());
      Assert.AreEqual(0, p.Count);

      IList<ISpell> spells = new List<ISpell>();
      spells.Add(new Spell());
      spells.Add(new Spell());
      p.AddSpells(spells);
      Assert.AreEqual(2, p.Count);

      ISpell s = new Spell();

      p.AddSpell(s);
      Assert.AreEqual(3, p.Count);

      p.AddSpell(s);
      Assert.AreEqual(3, p.Count);

      p.AddSpell(new Spell());
      Assert.AreEqual(4, p.Count);
    }
  }
}
