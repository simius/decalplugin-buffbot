﻿using System;
using System.Collections.Generic;
using System.Linq;
using Buffbot.Bot;
using Buffbot.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BuffbotTest.Bot
{
  [TestClass]
  public class CustomerTest
  {
    #region Profile

    internal class Profile : IProfile
    {
      public int Count
      {
        get { throw new NotImplementedException(); }
      }

      public ISpell GetNext()
      {
        throw new NotImplementedException();
      }

      public void AddSpells(System.Collections.Generic.IList<ISpell> spellList)
      {
        throw new NotImplementedException();
      }

      public void AddSpell(ISpell spell)
      {
        throw new NotImplementedException();
      }

      public string Name
      {
        get { throw new NotImplementedException(); }
      }

      public IList<string> Synonymes
      {
        get { throw new NotImplementedException(); }
      }

    }

    #endregion

    [TestMethod]
    public void TestGuid()
    {
      Customer c = new Customer(10, "Test");
      Customer cc = new Customer(15, "Test");

      Assert.AreEqual(10, c.Guid);
      Assert.AreEqual(15, cc.Guid);
    }

    [TestMethod]
    public void TestName()
    {
      Customer c = new Customer(10, "Test1");
      Customer cc = new Customer(15, "Test2");

      Assert.AreEqual("Test1", c.Name);
      Assert.AreEqual("Test2", cc.Name);
    }

    [TestMethod]
    public void TestGetNext()
    {
      Customer c = new Customer(1, "Test");

      IProfile p1 = new Profile();
      IProfile p2 = new Profile();

      c.AddProfile(p1);
      c.AddProfile(p2);

      IProfile p3 = c.GetNext();
      IProfile p4 = c.GetNext();

      Assert.AreEqual(p1, p3);
      Assert.AreEqual(p2, p4);
      Assert.AreNotEqual(p3, p4);
    }

    [TestMethod]
    public void TestAddProfile()
    {
      Customer c = new Customer(1, "Test");
      Assert.AreEqual(0, c.Count);
      Assert.AreEqual(1, c.AddProfile(new Profile()));
      Assert.AreEqual(1, c.Count);
      Assert.AreEqual(2, c.AddProfile(new Profile()));
      Assert.AreEqual(2, c.Count);
    }

    [TestMethod]
    public void TestRemoveProfile()
    {
      Customer c = new Customer(1, "Test");
      IProfile p1 = new Profile();
      IProfile p2 = new Profile();
      
      c.AddProfile(p1);
      c.AddProfile(p2);

      Assert.IsTrue(c.RemoveProfile(p1));
      Assert.AreEqual(1, c.Count);
      Assert.AreEqual(p2, c.GetNext());

    }

    [TestMethod]
    public void TestClearQueue()
    {
      IProfile p1 = new Profile();
      IProfile p2 = new Profile();

      ICustomer c = new Customer(1, "Test");
      c.AddProfile(p1);
      c.AddProfile(p2);
      Assert.AreEqual(2, c.Count);

      IProfile[] cc = c.ClearQueue();
      Assert.IsTrue(cc.Contains(p1));
      Assert.IsTrue(cc.Contains(p2));

      Assert.AreEqual(0, c.Count);
    }

    [TestMethod]
    public void TestCount()
    {
      Customer c = new Customer(1, "Test");
      Assert.AreEqual(0, c.Count);
      Assert.AreEqual(1, c.AddProfile(new Profile()));
      Assert.AreEqual(2, c.AddProfile(new Profile()));
      Assert.AreEqual(2, c.Count);
    }

  }
}
