﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Buffbot.Bot;
using Buffbot.Contracts;
using Newtonsoft.Json;

namespace Buffbot.Factories
{
  public static class SpellFactory
  {

    internal class SpellType
    {
      /// <summary>
      /// Spell type id.
      /// </summary>
      public int Id { get; set; }

      /// <summary>
      /// Spell type name.
      /// </summary>
      public string Name { get; set; }

      /// <summary>
      /// Spell level.
      /// </summary>
      public short Level { get; set; }
    }

    private static IDictionary<string, IList<SpellType>> spellTypes;

    /// <summary>
    /// Initializes the factory with the data it requires to work.
    /// </summary>
    /// <param name="data">Raw json data or a filepath.</param>
    /// <param name="isJson">If json data, this should be true.</param>
    public static void Init(string data, bool isJson = false)
    {
      if (!isJson)
      {
        using (StreamReader fileData = new System.IO.StreamReader(data))
        {
          data = fileData.ReadToEnd();
        }
      }

      spellTypes = JsonConvert.DeserializeObject<Dictionary<string, IList<SpellType>>>(data);
    }

    /// <summary>
    /// Get a spell (if existant) from the factory.
    /// </summary>
    /// <param name="name">Spell name.</param>
    /// <returns>Spell or null.</returns>
    public static ISpell GetSpell(string name)
    {
      foreach (KeyValuePair<string, IList<SpellType>> familyKeyValuePair in spellTypes)
      {
        foreach (SpellType spellType in familyKeyValuePair.Value)
        {
          if (spellType.Name == name)
          {
            return new Spell(spellType.Id, spellType.Name, spellType.Level);
          }
        }
      }

      return null;
    }

    /// <summary>
    /// Get a spell by spell id.
    /// </summary>
    /// <param name="id">Id of the spell.</param>
    /// <returns>Spell or null.</returns>
    public static ISpell GetSpell(int id)
    {
      foreach (KeyValuePair<string, IList<SpellType>> familyKeyValuePair in spellTypes)
      {
        foreach (SpellType spellType in familyKeyValuePair.Value)
        {
          if (spellType.Id == id)
          {
            return new Spell(spellType.Id, spellType.Name, spellType.Level);
          }
        }
      }

      return null;
    }

    /// <summary>
    /// Get all spells of a given family.
    /// </summary>
    /// <param name="family">Family of spells to fetch.</param>
    /// <returns>List of spells of the given family.</returns>
    public static IList<ISpell> GetSpellsByFamily(string family)
    {
      IList<ISpell> spells = new List<ISpell>();

      if (spellTypes.ContainsKey(family))
      {
        foreach (SpellType spellType in spellTypes[family])
        {
          spells.Add(new Spell(spellType.Id, spellType.Name, spellType.Level));
        }
      }

      return spells.OrderBy(spell => spell.Level).ToList();
    }


  }
}
