﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Buffbot.Bot;
using Buffbot.Contracts;
using Newtonsoft.Json;

namespace Buffbot.Factories
{
  public class ProfileFactory
  {
    internal class ProfileType
    {
      /// <summary>
      /// Name of the profile.
      /// </summary>
      public string Name { get; set; }

      /// <summary>
      /// Alternative names of the profile.
      /// </summary>
      public List<string> Synonymes { get; set; }

      /// <summary>
      /// Spell families used by the profile.
      /// </summary>
      public List<string> SpellFamilies { get; set; }
    }

    private static IList<ProfileType> profileTypes;

    /// <summary>
    /// Initializes the factory with the data it requires to work.
    /// </summary>
    /// <param name="data">Raw json data or a filepath.</param>
    /// <param name="isJson">If json data, this should be true.</param>
    /// <exception cref="InvalidDataException">Thrown on invalid settings data.</exception>
    public static void InitFactory(string data, bool isJson = false)
    {
      if (!isJson)
      {
        using (StreamReader fileData = new System.IO.StreamReader(data))
        {
          data = fileData.ReadToEnd();
        }
      }

      profileTypes = JsonConvert.DeserializeObject<IList<ProfileType>>(data);

      List<string> synonymes = new List<string>();
      foreach (ProfileType profileType in profileTypes)
      {
        synonymes.Add(profileType.Name);
        synonymes.AddRange(profileType.Synonymes);
      }

      if ((new HashSet<string>(synonymes)).Count != synonymes.Count)
      {
        throw new InvalidDataException(
          "Failed to initialize the profile factory class. One or more profiles share names or synonymes.");
      }
    }

    /// <summary>
    /// Fetches a Profile by name or any of the synonymes used in the profile.
    /// </summary>
    /// <param name="name"></param>
    /// <returns>Profile if found, else null.</returns>
    public static IProfile GetByName(string name)
    {
      ProfileType t = profileTypes.FirstOrDefault(p => p.Name == name);

      if (t != null)
      {
        return new Profile(t.Name, t.SpellFamilies, t.Synonymes);
      }

      foreach (ProfileType profileType in profileTypes)
      {
        if (profileType.Synonymes.Any(synonyme => synonyme == name))
        {
          t = profileType;
        }
      }

      return t == null ? null : new Profile(t.Name, t.SpellFamilies, t.Synonymes);
    }

  }
}
