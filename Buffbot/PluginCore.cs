using System;
using Buffbot.Utils;
using Decal.Adapter;
using Decal.Adapter.Wrappers;

namespace Buffbot
{
  /// <summary>
  /// PluginCore class, mainly used for setup and creation of wrapper classes.
  /// </summary>
  public partial class PluginCore : PluginBase
  {
    public static PluginHost DecalPluginHost { get; private set; }

    private Main plugin;


    protected override void Startup()
    {
      try
      {
        DecalPluginHost = this.Host;
        // Setup the plugin main class.
        this.plugin = new Main(Path);
        this.plugin.Startup();
      }
      catch (Exception ex)
      {
        Debug.Log(ex);
      }
    }

    protected override void Shutdown()
    {
      try
      {
        this.plugin.Shutdown();
      }
      catch (Exception ex)
      {
        Debug.Log(ex);
      }
    }
  }
}