﻿using System;
using System.Text.RegularExpressions;
using Buffbot.Chat;

namespace Buffbot.Services
{

  public static class PlayerChatExtractorService
  {
    private static readonly Regex chatNameRegex = new Regex(@"\<Tell\:IIDString\:([0-9]+)\:([A-Za-z -']+)\>([A-Za-z '-]+)\<\\Tell\>");

    /// <summary>
    /// Extract a player chat object from a raw chat message.
    /// </summary>
    /// <param name="chatMessage">Message to extract from.</param>
    /// <returns>PlayerChat object or null if none possible to extract.</returns>
    public static PlayerChat GetFromMessage(string chatMessage)
    {
      if (!chatNameRegex.IsMatch(chatMessage)) return null;

      string[] split = chatNameRegex.Split(chatMessage);
      if (split.Length < 3)
      {
        return null;
      }

      int id = 0;
      if (!Int32.TryParse(split[1], out id))
      {
        return null;
      }

      string name = split[2];
      string message = split[3];
      return new PlayerChat()
      {
        Name = name,
        Message = message,
        Guid = id
      };
    }

  }
}
