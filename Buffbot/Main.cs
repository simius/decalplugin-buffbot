﻿using Buffbot.Chat;
using Buffbot.Contracts;
using Buffbot.DecalWrappers;
using Buffbot.Utils;

namespace Buffbot
{
  public class Main : IPlugin
  {
    private readonly string path;
    private readonly DecalWrapper decal;

    private View.View view;
    
    
    public string Path { get { return this.path; } }

    public Main(string pluginPath)
    {
      this.path = pluginPath;
      this.decal = new DecalWrapper();
    }


    public void Startup()
    {
      Debug.InitLogger(Path, "errors.log");
      this.view = new View.View();
     
      


    }

    public void Shutdown()
    {
      this.view.ViewDestroy();
    }
  }
}
