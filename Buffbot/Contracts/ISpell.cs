﻿namespace Buffbot.Contracts
{
  public interface ISpell
  {
    /// <summary>
    /// Spell Id.
    /// </summary>
    int Id { get; }

    /// <summary>
    /// Name of the spell.
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Level of the spell.
    /// </summary>
    short Level { get; }
  }
}
