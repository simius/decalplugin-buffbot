﻿using System.Collections.Generic;

namespace Buffbot.Contracts
{
  public interface IProfile
  {
    /// <summary>
    /// Name of the profile.
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Synonymes for the profile name
    /// </summary>
    IList<string> Synonymes { get; }

    /// <summary>
    /// Spellcount left in the queue.
    /// </summary>
    int Count { get; }

    /// <summary>
    /// Get next spell in queue and remove it from the queue.
    /// </summary>
    /// <returns></returns>
    ISpell GetNext();

    /// <summary>
    /// Add spells to the spell list of the profile.
    /// </summary>
    /// <param name="spellList">List of spells to add.</param>
    void AddSpells(IList<ISpell> spellList);

    /// <summary>
    /// Add a single spell to the end of the spell queue.
    /// </summary>
    /// <param name="spell">Spell to add.</param>
    void AddSpell(ISpell spell);
  }
}
