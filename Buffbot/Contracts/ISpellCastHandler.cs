﻿using System;

namespace Buffbot.Contracts
{
  public delegate void OnSpellCastComplete(SpellCastCompleteEventArgs args);

  // TODO: Tests needed for this interface.

  /// <summary>
  /// Arguments for the SpellCastComplete delegate.
  /// </summary>
  public class SpellCastCompleteEventArgs : EventArgs
  {
    public bool Fizzle { get; internal set; }
    public ITarget Target { get; internal set; }
    public ISpell Spell { get; internal set; }
  }

  public interface ISpellCastHandler
  {
    /// <summary>
    /// Cast the given spell on the given target and fire callback when spell cast is complete.
    /// </summary>
    /// <param name="target">Target.</param>
    /// <param name="spell">Spell.</param>
    /// <param name="onDoneCallback">Callback to fire on complete.</param>
    /// <returns>Returns false if the bot is bussy.</returns>
    bool Invoke(ITarget target, ISpell spell, OnSpellCastComplete onDoneCallback);

    /// <summary>
    /// Clean up the object.
    /// </summary>
    void Destroy();
  }
}