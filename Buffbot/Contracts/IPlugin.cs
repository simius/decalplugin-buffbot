﻿namespace Buffbot.Contracts
{
  public interface IPlugin
  {
    void Startup();
    void Shutdown();
  }
}
