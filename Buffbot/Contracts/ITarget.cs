﻿namespace Buffbot.Contracts
{
  public interface ITarget
  {
    /// <summary>
    /// Target name.
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Target Globaly unique id.
    /// </summary>
    int Guid { get; }
  }
}