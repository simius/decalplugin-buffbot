﻿namespace Buffbot.Contracts
{
  public interface ICustomer
  {
    /// <summary>
    /// Number of Profiles left in the queue.
    /// </summary>
    int Count { get; }

    /// <summary>
    /// Customer Globaly unique ID.
    /// </summary>
    int Guid { get; }

    /// <summary>
    /// Get the next profile in the queue and removes it from the queue.
    /// </summary>
    /// <returns>Profile.</returns>
    IProfile GetNext();

    /// <summary>
    /// Add a profile to the customers profile queue.
    /// </summary>
    /// <param name="profile">Profile to add.</param>
    /// <returns>Number of profiles in the customers queue, including the newly added profile.</returns>
    int AddProfile(IProfile profile);

    /// <summary>
    /// Remove a profile from the queue.
    /// </summary>
    /// <param name="profile">Profile to remove.</param>
    /// <returns>True if removed successfully.</returns>
    bool RemoveProfile(IProfile profile);
    
    /// <summary>
    /// Clear the customers profile queue.
    /// </summary>
    /// <returns>List of profiles removed from the queue as an array.</returns>
    IProfile[] ClearQueue();
  }
}
