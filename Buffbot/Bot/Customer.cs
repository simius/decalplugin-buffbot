﻿using System;
using System.Collections.Generic;
using Buffbot.Contracts;

namespace Buffbot.Bot
{
  public class Customer : ICustomer, ITarget, IEquatable<Customer>
  {
    private readonly int guid;
    private readonly string name;
    private readonly List<IProfile> profiles;

    /// <summary>
    /// Customer Name.
    /// </summary>
    public string Name { get { return this.name; } }

    /// <summary>
    /// Customer Globaly unique ID.
    /// </summary>
    public int Guid { get { return this.guid; } }

    /// <summary>
    /// Number of Profiles left in the queue.
    /// </summary>
    public int Count { get { return this.profiles.Count; } }

    public Customer(int guid, string name)
    {
      this.name = name;
      this.guid = guid;
      this.profiles = new List<IProfile>();
    }
    
    /// <summary>
    /// Get the next profile in the queue and removes it from the queue.
    /// </summary>
    /// <returns>Profile.</returns>
    public IProfile GetNext()
    {
      if (this.profiles.Count == 0) return null;

      IProfile next = this.profiles[0];
      this.profiles.RemoveAt(0);
      return next;
    }

    /// <summary>
    /// Add a profile to the customers profile queue.
    /// </summary>
    /// <param name="profile">Profile to add.</param>
    /// <returns>Number of profiles in the customers queue, including the newly added profile.</returns>
    public int AddProfile(IProfile profile)
    {
      if (this.profiles.Contains(profile)) return - 1;

      this.profiles.Add(profile);
      return this.profiles.Count;
    }

    /// <summary>
    /// Remove a profile from the queue.
    /// </summary>
    /// <param name="profile">Profile to remove.</param>
    /// <returns>True if removed successfully.</returns>
    public bool RemoveProfile(IProfile profile)
    {
      return this.profiles.Remove(profile);
    }

    /// <summary>
    /// Clear the customers profile queue.
    /// </summary>
    /// <returns>List of profiles removed from the queue as an array.</returns>
    public IProfile[] ClearQueue()
    {
      IProfile[] tmp = this.profiles.ToArray();
      this.profiles.Clear();
      return tmp;
    }

    #region IEquatable<Customer> Members

    public bool Equals(Customer other)
    {
      return other.guid == this.guid;
    }

    #endregion


  }
}
