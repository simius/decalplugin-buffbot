﻿using System;
using System.Collections.Generic;
using Buffbot.Contracts;

namespace Buffbot.Bot
{
  public class Profile : IProfile, IEquatable<IProfile>
  {
    private readonly IList<ISpell> spells;
    private readonly IList<string> synonymes;
    private readonly IList<string> spellFamilies;
    private readonly string name;

    /// <summary>
    /// Name of the profile.
    /// </summary>
    public string Name 
    {
      get { return this.name; }
    }

    /// <summary>
    /// Synonymes for the profile name
    /// </summary>
    public IList<string> Synonymes
    {
      get { return this.synonymes; }
    }

    /// <summary>
    /// Spell families the profile consists of.
    /// </summary>
    public IList<string> SpellFamilies
    {
      get { return this.spellFamilies; }
    } 

    /// <summary>
    /// Spellcount left in the queue.
    /// </summary>
    public int Count
    {
      get { return this.spells.Count; }
    }

    public Profile(string name, IList<string> spellFamilies, IList<string> synonymes = null)
    {
      this.spells = new List<ISpell>();
      this.name = name;
      this.spellFamilies = spellFamilies;
      this.synonymes = (synonymes ?? new List<string>());

    }

    /// <summary>
    /// Add spells to the spell list of the profile.
    /// </summary>
    /// <param name="spellList">List of spells to add.</param>
    public void AddSpells(IList<ISpell> spellList)
    {
      foreach (ISpell spell in spellList)
      {
        this.AddSpell(spell);
      }
    }
    
    /// <summary>
    /// Add a single spell to the end of the spell queue.
    /// </summary>
    /// <param name="spell">Spell to add.</param>
    public void AddSpell(ISpell spell)
    {
      if (!this.spells.Contains(spell))
      {
        this.spells.Add(spell);
      }
    }

    /// <summary>
    /// Get next spell in queue and remove it from the queue.
    /// </summary>
    /// <returns></returns>
    public ISpell GetNext()
    {
      if (this.spells.Count == 0) return null;

      ISpell spell = this.spells[0];
      this.spells.RemoveAt(0);
      return spell;
    }


    #region IEquatable<Profile> Members

    public bool Equals(IProfile other)
    {
      return this.name != other.Name;
    }

    #endregion

  }
}
