﻿using System.Collections.Generic;
using Buffbot.Contracts;

namespace Buffbot.Bot
{
  public class CustomerQueue
  {
    private readonly List<ICustomer> customers;

    /// <summary>
    /// Current number of customers.
    /// </summary>
    public int Count { get { return this.customers.Count; } }

    /// <summary>
    /// Create a customer queue.
    /// </summary>
    public CustomerQueue()
    {
      this.customers = new List<ICustomer>();
    }

    /// <summary>
    /// Add a customer to the queue.
    /// </summary>
    /// <param name="customer">Customer to add.</param>
    /// <returns>-1 if customer was already in the queue else the length of the queue (including the new customer).</returns>
    public int AddCustomer(ICustomer customer)
    {
      if (this.customers.Contains(customer))
      {
        return -1;
      }

      this.customers.Add(customer);
      return this.customers.Count;
    }

    /// <summary>
    /// Remove a customer from the queue.
    /// </summary>
    /// <param name="customer">Customer to remove.</param>
    /// <returns>True iof successfully removed, else false.</returns>
    public bool RemoveCustomer(ICustomer customer)
    {
      return this.customers.Remove(customer);
    }

    /// <summary>
    /// Get next customer in queue and remove it from the queue.
    /// </summary>
    /// <returns>Customer or null if queue is empty.</returns>
    public ICustomer GetNext()
    {
      if (this.customers.Count == 0) return null;

      ICustomer next = this.customers[0];
      this.customers.RemoveAt(0);
      return next;
    }

    /// <summary>
    /// Clear the queue and return the customers as an array.
    /// </summary>
    /// <returns>Array of customers removed from the queue.</returns>
    public ICustomer[] ClearQueue()
    {
      ICustomer[] temp = this.customers.ToArray();
      this.customers.Clear();
      return temp;
    }

  }
}
