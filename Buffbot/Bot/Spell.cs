﻿using System;
using Buffbot.Contracts;

namespace Buffbot.Bot
{
  public class Spell :ISpell, IEquatable<ISpell>
  {
    private readonly string name;
    private readonly int id;
    private readonly short level;

    /// <summary>
    /// Spell Id.
    /// </summary>
    public int Id
    {
      get { return this.id; }
    }

    /// <summary>
    /// Name of the spell.
    /// </summary>
    public string Name
    {
      get { return this.name; }
    }

    /// <summary>
    /// Spell level.
    /// </summary>
    public short Level
    {
      get { return this.level; }
    }

    /// <summary>
    /// Spell constructor.
    /// </summary>
    /// <param name="id">Id of the spell.</param>
    /// <param name="name">Name of the spell.</param>
    /// <param name="level">Spell level.</param>
    public Spell(int id, string name, short level)
    {
      this.id = id;
      this.name = name;
      this.level = level;
    }

    #region IEquatable<ISpell> Members

    public bool Equals(ISpell other)
    {
      return (this.id == other.Id) && (this.name == other.Name);
    }

    #endregion
  }
}
