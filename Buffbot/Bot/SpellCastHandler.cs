﻿using System.Text.RegularExpressions;
using Buffbot.Contracts;
using Buffbot.Utils;
using Decal.Adapter;

namespace Buffbot.Bot
{
  /// <summary>
  /// Spell casting handler. Will make sure that the spell is cast and that, when it is done casting, calls the supplied callback.
  /// </summary>
  internal class SpellCastHandler : ISpellCastHandler
  {
    private static readonly Regex spellCastCompleteRegex = new Regex(@"You cast SpellName (and|on yourself|on CharacterName)");
    private readonly CoreManager core;

    private ISpell currentSpell = null;
    private ITarget currentTarget = null;
    private OnSpellCastComplete callback = null;
    private bool bussy = false;

    public SpellCastHandler(CoreManager core)
    {
      this.core = core;
      // Set up event listener for chat to catch spell cast complete.
      this.core.ChatBoxMessage += CurrentOnChatBoxMessage;
    }

    private void CurrentOnChatBoxMessage(object sender, ChatTextInterceptEventArgs chatTextInterceptEventArgs)
    {
      if (chatTextInterceptEventArgs.Color != 0x0000000000020080) return;

      Debug.Log("Spell cast message recieved.");

      SpellCastCompleteEventArgs args = new SpellCastCompleteEventArgs()
      {
        Target = this.currentTarget,
        Spell = this.currentSpell,
      };
      if (chatTextInterceptEventArgs.Text.Contains("Your spell fizzled."))
      {
        args.Fizzle = true;
      }
      else if (!spellCastCompleteRegex.IsMatch(chatTextInterceptEventArgs.Text))
      {
        Debug.Log("Spell cast message was not usefull for the bot.");
        return;
      }
      else
      {
        Debug.Log("Spell cast successfull and complete.");
        args.Fizzle = false;
      }

      this.currentTarget = null;
      this.currentSpell = null;
      this.bussy = false;
      this.callback(args);
    }

    /// <summary>
    /// Cast the given spell on the given target and fire callback when spell cast is complete.
    /// </summary>
    /// <param name="target">Target.</param>
    /// <param name="spell">Spell.</param>
    /// <param name="onDoneCallback">Callback to fire on complete.</param>
    /// <returns>Returns false if the bot is bussy.</returns>
    public bool Invoke(ITarget target, ISpell spell, OnSpellCastComplete onDoneCallback)
    {
      Debug.Log("Trying to cast spell (" + spell.Name + ") on " + target.Name + ".");

      if (this.bussy || (this.core.Actions.BusyState != 0))
      {
        Debug.Log("But I was bussy.");
        return false;
      }

      this.bussy = true;
      this.currentSpell = spell;
      this.currentTarget = target;
      this.callback = onDoneCallback;

      Debug.Log("Was not bussy, all parameters set up and ready. Casting...");
      this.core.Actions.CastSpell(spell.Id, target.Guid);
      return true;
    }

    /// <summary>
    /// Clean up the object.
    /// </summary>
    public void Destroy()
    {
      this.core.ChatBoxMessage -= CurrentOnChatBoxMessage;
    }
  }
}
