﻿namespace Buffbot.Chat
{
  public class PlayerChat
  {
    /// <summary>
    /// The chat message.
    /// </summary>
    public string Message { get; set; }

    /// <summary>
    /// Name of the player sending chat.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Globaly unique id of the player sending chat.
    /// </summary>
    public int Guid { get; set; }

    /// <summary>
    /// Chat channel.
    /// </summary>
    public int Channel { get; set; }
  }
}
