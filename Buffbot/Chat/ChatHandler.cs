﻿using Buffbot.Services;

namespace Buffbot.Chat
{
  public class ChatHandler
  {
    public ChatHandler()
    {
      
    }

    public void HandleChat(string message, int channel)
    {
      PlayerChat chat = PlayerChatExtractorService.GetFromMessage(message);
      chat.Channel = channel;
    }

  }
}
