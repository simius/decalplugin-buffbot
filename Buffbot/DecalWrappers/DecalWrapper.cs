﻿using System;
using Buffbot.Bot;
using Buffbot.Contracts;
using Decal.Adapter;
using Decal.Adapter.Wrappers;

namespace Buffbot.DecalWrappers
{
  
  public class DecalWrapper
  {
    private readonly ISpellCastHandler spellCastHandler;

    public DecalWrapper()
    {
      this.spellCastHandler = new SpellCastHandler(CoreManager.Current); 
    }

    /// <summary>
    /// Cast a spell on a given target. Callback will be fired when spell cast is complete.
    /// </summary>
    /// <param name="target">Target to cast spell on.</param>
    /// <param name="spell">Spell to cast on target.</param>
    /// <param name="callback">Callback to fire on done.</param>
    /// <returns>False if already casting a spell or bussy for any other reason.</returns>
    public bool CastSpell(ITarget target, ISpell spell, OnSpellCastComplete callback)
    {
      return this.spellCastHandler.Invoke(target, spell, callback);
    }

  }
}
