﻿using System;
using System.Collections.Generic;

// TODO: Fix logger.

namespace Buffbot.Utils
{
  internal enum LoggerTags
  {
    MESSAGE,
    DEBUG,
    INFO,
    WARNING,
    ERROR,
    EXCEPTION
  }

  internal partial class Debug
  {
    internal class Logger
    {
      private string fileName;
      private string filePath;
      private string messageTemplate;
      private string longMessageTemplate;
      private Dictionary<LoggerTags, bool> states;

      private readonly object lockObject = new object();

      internal Logger(string filePath, string fileName)
      {
        


      }

      internal void Log(string message, LoggerTags tag = LoggerTags.DEBUG)
      {


      }

      internal void Log(Exception ex)
      {

      }
    }

    private static Logger logger;


    public static void InitLogger(string filePath, string fileName)
    {
      logger = new Logger(filePath, fileName);
    }

    /// <summary>
    /// Logg a message with a tag to file.
    /// </summary>
    /// <param name="message">Message to log.</param>
    /// <param name="tag">Tag to use.</param>
    public static void Log(string message, LoggerTags tag = LoggerTags.DEBUG)
    {
      logger.Log(message, tag);
    }

    /// <summary>
    /// Log an exception to file.
    /// </summary>
    /// <param name="ex">Exception to log.</param>
    public static void Log(Exception ex)
    {
      logger.Log(ex);
    }

    
  }


}
