using System;
using Buffbot.Contracts;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using MyClasses.MetaViewWrappers;

namespace Buffbot.View
{
  /// <summary>
  /// View class.
  /// 
  /// Observe:
  /// This class have to use some decal specific stuff, so it will access the Host 
  /// from the IHost object passed in through the constructor
  /// </summary>
  internal class View
  {
    private IView view;
    private readonly PluginHost host;

#if DEBUG
    private readonly IView debugview;
    private IButton reloadUiButton;
#endif

    /// <summary>
    /// Constructor.
    /// </summary>
    public View()
    {
#if DEBUG
      this.host = PluginCore.DecalPluginHost;
      this.debugview = ViewSystemSelector.CreateViewResource(this.host as PluginHost, "Buffbot.View.debug.xml");
      DebugViewSetup();
#endif
      this.view = ViewSystemSelector.CreateViewResource(this.host as PluginHost, "Buffbot.View.view.xml");
    }

#if DEBUG

    /// <summary>
    /// Setup code for the debug view, will only be done if plugin is compiled with debug flag.
    /// </summary>
    private void DebugViewSetup()
    {
      this.reloadUiButton = this.GetControl("reload", this.debugview) as IButton;

      if (this.reloadUiButton == null)
      {
        throw new NullReferenceException("The reload ui button was not possible to fetch from the debug view.");
      }

      this.reloadUiButton.Hit += delegate(object sender, EventArgs args) {
        this.view.Dispose();
        this.view = ViewSystemSelector.CreateViewResource(this.host as PluginHost, "Buffbot.View.view.xml");
      };
    }

#endif

    /// <summary>
    /// Fetch a IControl from the main view or, if passed in, another view.
    /// </summary>
    /// <param name="name">Name of control.</param>
    /// <param name="v">View to fetch control from, defaults to the main view.</param>
    /// <returns>A IControl object if found, else null.</returns>
    private IControl GetControl(string name, IView v = null)
    {
      if (v == null)
      {
        v = this.view;
      }

      return v[name];
    }

    /// <summary>
    /// Destroy on end.
    /// </summary>
    public void ViewDestroy()
    {
      this.view.Dispose();
    }
  }
}